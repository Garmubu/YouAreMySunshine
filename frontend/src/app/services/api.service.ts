import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../interfaces/User';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  SERVER_URL= "http://localhost:5800";
  constructor(private httpClient: HttpClient ) {
    
  }
   
   postInscription(user: User){
      return this.httpClient.post<User>(this.SERVER_URL+"/signin", user);
    }
    postConnexion(user: User){
      return this.httpClient.post<User>(this.SERVER_URL+"/signup", user);
    }
}
