import { Routes } from '@angular/router';
import { SigninComponent } from './component/signin/signin.component';

export const routes: Routes = [
    { path: "login", component: SigninComponent }
];
