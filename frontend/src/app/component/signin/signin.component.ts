import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ApiService } from '../../services/api.service';
import { User } from '../../interfaces/User';
import {NgIf} from "@angular/common";

@Component({
  selector: 'app-signin',
  standalone:true,
  imports: [FormsModule, NgIf],

  templateUrl: './signin.component.html',
  styleUrl: './signin.component.css'
})
export class SigninComponent implements OnInit{

    form={
        email:'',
        password:''
    };
    user:User={
        email:undefined,
        password:undefined
    };
  formError: string | null = null;
  constructor(private api : ApiService){}
  ngOnInit():void{}
  connexion(form: any){
    if (form.invalid) {
      this.formError = "Veuillez remplir tous les champs.";
    } else {
      this.formError = "Connection reussit";
    }
    this.user={
        email: this.form.email,
        password: this.form.password
    }
    console.log(this.user)
    this.api.postConnexion(this.user)
  }
}
