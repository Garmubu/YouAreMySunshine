describe('signin', () => {
  beforeEach(() => {

    cy.visit('/login')

  })
  it('verification du formulaire ', () => {
    cy.get('form').should('exist');
    cy.get('[data-cy=email]').should('exist');
    cy.get('[data-cy=password]').should('exist');
    cy.get("label").should('exist');
    cy.get('[data-cy="connexion"]').should('exist');
  })
  it('affichage du message succée', () => {
    cy.get('[data-cy=email]').type('ali.guicheri@epsi.com');
    cy.get('[data-cy=password]').type('123456789');
    cy.get('[data-cy=connexion]').click()
    cy.get('[data-cy=formResult]').contains('Connection reussit')
  })
  it("affichage les different message d'erreur", () => {
    cy.get('[data-cy=email]').click();
    cy.get('[data-cy=password]').click();
    cy.get('[data-cy=connexion]').click();
    cy.get('[data-cy="emailRequired"]').contains("L'email est requis")
    cy.get('[data-cy="passwordRequired"]').contains("Le mot de passe est requis.")
  })
  it("affiche message d'erreur pour l'email", () => {
    cy.get('[data-cy=email]').type('ali.guicheri@epsi.com');
    cy.get('[data-cy=password]').click().wait(1000);
    cy.get('[data-cy=connexion]').click();
    cy.get('[data-cy=connexion]').click();
    cy.get('[data-cy="passwordRequired"]').contains("Le mot de passe est requis.")
    cy.get('[data-cy=formResult]').contains('Veuillez remplir tous les champs.')
  })
})
