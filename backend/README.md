TP de Emerton Billel et Guicheri Ali

Mode d'emploi pour lancer l'app:

Se placer a la racine du projet et lancer:
dotnet tool install --global dotnet-ef

Ensuite se deplacer via dans GestionHotel.Apis et lancer
dotnet add package Microsoft.EntityFrameworkCore.Design

Puis:
dotnet ef migrations add InitialCreate

Vous pouvez a present lancer l'api a partir du ficher program.cs

http://localhost:5000/api/Users/signin
JSON pour créer un user:
{
"firstName":"Remi",
"lastName":"Henache",
"email":"Remi.Henache@hotmail.fr",
"password":"monmdpbienlong"
}
http://localhost:5000/api/Users/signup
JSON pour se connecter:
{
"email":"Remi.Henache@hotmail.fr",
"password":"monmdpbienlong"
}