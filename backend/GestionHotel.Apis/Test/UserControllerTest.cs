﻿using System.Net;
using FluentAssertions;
using GestionHotel.Apis.Controllers;
using GestionHotel.Apis.Models;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;
namespace GestionHotel.Apis.Tests {
    public class UsersControllerTests : IClassFixture<WebApplicationFactory<Program>>
    {
        private readonly HttpClient _client;

        public UsersControllerTests(WebApplicationFactory<Program> factory)
        {
            _client = factory.CreateClient();
        }

        [Fact]
        public async Task Register_ValidInput_ShouldReturnOk()
        {
            // with
            var user = new User("Doe", "John", "john.doe@example.com", "SecurePassword123");


            // when
            var response = await _client.PostAsJsonAsync("/api/users/signin", user);

            // then
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var content = await response.Content.ReadAsStringAsync();
            content.Should().Be("User created");
        }

        [Fact]
        public async Task Register_InvalidInput_ShouldReturnBadRequest()
        {
            // with
            var userWrong = new User("Doe", "John", "john.doe@example.com", "");

            // then
            var response = await _client.PostAsJsonAsync("/api/users/signin", userWrong);

            // then
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
            var content = await response.Content.ReadAsStringAsync();
            content.Should().Be("Lacks informations");
        }

        [Fact]
        public async Task Login_ValidInput_ShouldReturnUser()
        {
            // with
            var user = new User("Doe", "John", "john.doe@example.com", "SecurePassword123");

            await _client.PostAsJsonAsync("/api/users/signin", user); // register the user

            var loginInput = new LoginInput("john.doe@example.com", "SecurePassword123");

            // when
            var response = await _client.PostAsJsonAsync("/api/users/signup", loginInput);

            // then
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var userRes = await response.Content.ReadFromJsonAsync<UserWithoutPassword>();
            userRes.Should().NotBeNull();
            userRes.Email.Should().Be(loginInput.Email);
        }

        [Fact]
        public async Task Login_InvalidInput_ShouldReturnUnauthorized()
        {
            // with
            var user = new User("Doe", "John", "john.doe@example.com", "SecurePassword123");

            await _client.PostAsJsonAsync("/api/users/signin", user); // register the user 

            var loginInput = new LoginInput("john.doe@example.com", "SecurePassword123");


            // when
            var response = await _client.PostAsJsonAsync("/api/users/signup", loginInput);

            // then
            response.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
        }
    }
}

