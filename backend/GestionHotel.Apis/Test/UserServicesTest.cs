﻿using GestionHotel.Apis;
using GestionHotel.Apis.Models;
using GestionHotel.Apis.Services;
using Microsoft.EntityFrameworkCore;
using Moq;
using Xunit;

namespace GestionHotel.Apis.Tests {
    public class UserServiceTests
    {
        private readonly UserService _userService;
        private readonly Mock<DatabaseContext> _dbContextMock;
        private readonly Mock<DbSet<User>> _userDbSetMock;

        public UserServiceTests()
        {
            _dbContextMock = new Mock<DatabaseContext>();
            _userDbSetMock = new Mock<DbSet<User>>();
            _dbContextMock.Setup(db => db.Users).Returns(_userDbSetMock.Object);

            _userService = new UserService();
        }

        [Fact]
        public void ApplyUserVerifications_ValidUser_ReturnsTrue()
        {
            var user = new User("Doe", "John", "john.doe@example.com", "SecurePassword123");
            _userDbSetMock.Setup(m => m.AsQueryable()).Returns(new List<User>().AsQueryable());

            var result = _userService.ApplyUserVerifications(user);

            Assert.True(result);
        }

        [Fact]
        public void ApplyUserVerifications_PasswordTooShort_ReturnsFalse()
        {
            var user = new User("Doe", "John", "john.doe@example.com", "short");
            _userDbSetMock.Setup(m => m.AsQueryable()).Returns(new List<User>().AsQueryable());

            var result = _userService.ApplyUserVerifications(user);

            Assert.False(result);
        }

        [Fact]
        public void ApplyUserVerifications_InvalidEmail_ReturnsFalse()
        {
            var user = new User("Doe", "John", "invalid-email", "SecurePassword123");
            _userDbSetMock.Setup(m => m.AsQueryable()).Returns(new List<User>().AsQueryable());

            var result = _userService.ApplyUserVerifications(user);

            Assert.False(result);
        }
    }
}

