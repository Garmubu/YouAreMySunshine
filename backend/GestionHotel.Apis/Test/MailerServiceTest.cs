﻿using GestionHotel.Apis.Services;
using Xunit;

namespace GestionHotel.Apis.Tests
{
    public class EmailServiceTests
    {
        [Fact]
        public void SendEmail_ValidEmailAndSubject_WritesExpectedOutput()
        {
            // with
            var emailService = new MailerService();
            var email = "test@example.com";
            var subject = "Test Subject";
            var expectedOutput = $"Sending {subject} to {email}...{Environment.NewLine}";

            using (var sw = new StringWriter())
            {
                Console.SetOut(sw);

                // when
                emailService.SendEmail(email, subject);

                // then
                var result = sw.ToString();
                Assert.Equal(expectedOutput, result);
            }
        }
    }
}
