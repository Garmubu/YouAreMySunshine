﻿using System.Net;
using FluentAssertions;
using GestionHotel.Apis.Models;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;

namespace GestionHotel.Apis.Tests
{
    public class RoomsControllerTests : IClassFixture<WebApplicationFactory<Program>>
    {
        private readonly HttpClient _client;

        public RoomsControllerTests(WebApplicationFactory<Program> factory)
        {
            _client = factory.CreateClient();
        }

        [Fact]
        public async Task ShowAvailableRooms_AsClient_ShouldReturnAvailableRooms()
        {
            var response = await _client.GetAsync("/api/rooms/available?token=Client");

            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var rooms = await response.Content.ReadFromJsonAsync<List<RoomResult>>();
            rooms.Should().NotBeNull();

        }

        [Fact]
        public async Task ShowAvailableRooms_AsReceptionist_ShouldReturnDetailedAvailableRooms()
        {
            var response = await _client.GetAsync("/api/rooms/available?token=Receptionist");

            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var rooms = await response.Content.ReadFromJsonAsync<List<RoomResult>>();
            rooms.Should().NotBeNull();

        }

        [Fact]
        public async Task ShowAvailableRooms_InvalidToken_ShouldReturnBadRequest()
        {
            var response = await _client.GetAsync("/api/rooms/available?token=Invalid");

            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [Fact]
        public async Task ShowUncleanRooms_AsHousekeeper_ShouldReturnUncleanRooms()
        {

            var response = await _client.GetAsync("/api/rooms/unclean");

            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var rooms = await response.Content.ReadFromJsonAsync<List<Room>>();
            rooms.Should().NotBeNull();

        }

        [Fact]
        public async Task CleanRoom_AsHousekeeper_ShouldReturnOk()
        {

            var roomId = "random-room-id";
            var response = await _client.PatchAsync($"/api/rooms/clean/{roomId}", null);

            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var content = await response.Content.ReadAsStringAsync();
            content.Should().Be("Room cleaned");
        }
    }

}

