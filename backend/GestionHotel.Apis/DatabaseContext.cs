﻿using GestionHotel.Apis.Models;
using Microsoft.EntityFrameworkCore;

namespace GestionHotel.Apis;

public class DatabaseContext: DbContext
{
    public DbSet<User> Users { get; set; }
    public DbSet<Booking> Bookings { get; set; }
    public DbSet<Room> Rooms { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder options)
        => options.UseSqlite($"Data Source=./GestionHotel.db");
}