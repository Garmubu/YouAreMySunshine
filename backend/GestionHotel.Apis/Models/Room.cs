﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using GestionHotel.Apis.Models.Enums;

namespace GestionHotel.Apis.Models;

public class Room : BaseModel
{
    [Required]
    public RoomType Type { get; set; }

    [Required]
    public double Price { get; set; }

    [Required]
    public RoomState State { get; set; }

    [Required]
    public int Capacity { get; set; }

    public bool IsClean { get; private set; }

    public bool IsAvailable { get; private set; }

    public bool IsCurrentlyOccupied { get; private set; }


    public Room(RoomType type, double price, RoomState state, int capacity)
    {
        Type = type;
        Price = price;
        State = state;
        this.Capacity = capacity;
        this.IsClean = true;
        this.IsAvailable = true;
        this.IsCurrentlyOccupied = false;
    }

    public void CleanRoom()
    {
        this.IsClean = true;
    }
    
    public void MarkRoomForCleaning()
    {
        this.IsClean = false;
    }

    public void ChangeRoomAvailability(bool available)
    {
        this.IsAvailable = available;
    }
    
    public void ChangeRoomOccupation(bool occupied)
    {
        this.IsCurrentlyOccupied = occupied;
    }
}