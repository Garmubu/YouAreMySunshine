﻿namespace GestionHotel.Apis.Models.Enums;

public enum RoomState
{
    Clean,
    Redone,
    ToBeRedone,
    New,
    BigDamage
}