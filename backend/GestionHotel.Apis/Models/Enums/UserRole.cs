﻿namespace GestionHotel.Apis.Models.Enums;

public enum UserRole
{
    Client,
    Receptionist,
    Housekeeper,
    Admin
}