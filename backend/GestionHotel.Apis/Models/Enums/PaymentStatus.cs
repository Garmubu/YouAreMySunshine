﻿namespace GestionHotel.Apis.Models.Enums;

public enum PaymentStatus
{
    AwaitingPaymentMethodChoice,
    AwaitingCashPayment,
    Paid
}