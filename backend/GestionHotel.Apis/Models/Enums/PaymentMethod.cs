﻿namespace GestionHotel.Apis.Models.Enums;

public enum PaymentMethod
{
    Paypal,
    Stripe,
    Other
}