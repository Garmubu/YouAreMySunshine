﻿namespace GestionHotel.Apis.Models.Enums;

public enum BookingStatus
{
    Created,
    Canceled,
    Done
}