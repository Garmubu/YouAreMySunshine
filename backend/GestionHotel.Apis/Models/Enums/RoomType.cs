﻿namespace GestionHotel.Apis.Models.Enums;

public enum RoomType
{
    Simple,
    Double,
    Suite,
    Penthouse
}