﻿using GestionHotel.Apis.Models;
using GestionHotel.Apis.Models.Enums;
using GestionHotel.Apis.Services;
using Microsoft.AspNetCore.Mvc;

namespace GestionHotel.Apis.Controllers;

[ApiController]
[Route("api/[controller]")]
public class BookingsController : ControllerBase
{
    private readonly BookingsService _bookingsService = new BookingsService();

    //permet de reserver une chambre
    [HttpPost("book")]
    [CustomAuthorization(UserRole.Receptionist, UserRole.Client)]
    public IActionResult BookRoom([FromBody] BookingReservationInput input)
    {
        _bookingsService.AttemptBooking(input);
        return Ok(input);
    }
    
    //permet d'encaisser un client selon son moyen de paiement
    [CustomAuthorization(UserRole.Receptionist)]
    [HttpPatch("handle-client-arrival")]
    public IActionResult HandleClientArrival([FromBody] ClientInput input)
    {
        _bookingsService.HandleClientArrival(input.Email, input.Payment ?? null);
        return Ok(input);
    }
    
    //permet de liberer la chambre en bdd
    [CustomAuthorization(UserRole.Receptionist)]
    [HttpPatch("handle-client-departure")]
    public IActionResult HandleClientDeparture([FromBody] ClientInput input)
    {
        _bookingsService.HandleClientDeparture(input.Email, input.Payment);
        return Ok(input);
    }

    //permet d'annuler un rendez-vous
    [CustomAuthorization(UserRole.Receptionist, UserRole.Client)]
    [HttpPatch("cancel/{bookingId}")]
    public IActionResult CancelBooking(string bookingId)
    {
        _bookingsService.CancelBooking(bookingId);
        return Ok(bookingId);
    }
    

}