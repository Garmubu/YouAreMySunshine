﻿using System.ComponentModel.DataAnnotations;
using GestionHotel.Apis.Models;
using GestionHotel.Apis.Models.Enums;


public class RoomResult: BaseModel
{
    public RoomType Type { get; set; }
    public double Price { get; set; }
    public RoomState State { get; set; }
    public int Capacity { get; set; }
    public bool IsClean { get; private set; }
    public bool IsAvailable { get; private set; }

    public RoomResult(string id, RoomType type, double price, int capacity)
    {
        Id = id;
        Type = type;
        Price = price;
        Capacity = capacity;
    }

    public RoomResult(string id, RoomType type, double price, RoomState state, int capacity, bool isRoomClean, bool isRoomAvailable)
    {
        Id = id;
        Type = type;
        Price = price;
        State = state;
        Capacity = capacity;
        IsClean = isRoomClean;
        IsAvailable = isRoomAvailable;
    }
}