﻿using GestionHotel.Apis.Models;
using GestionHotel.Apis.Models.Enums;
using GestionHotel.Apis.Services;
using Microsoft.AspNetCore.Mvc;

namespace GestionHotel.Apis.Controllers;

[ApiController]
[Route("api/[controller]")]
public class RoomsController : ControllerBase
{
    private readonly RoomsService _roomsService = new RoomsService();

    //retourne les chambres libres
    [HttpGet("available")]
    [CustomAuthorization(UserRole.Receptionist, UserRole.Client)]
    public IActionResult ShowAvailableRooms(string token)
    {
        var rooms = _roomsService.Select(r => r.IsAvailable);
        switch (token)
        {
            case "Client":
                return Ok(rooms.Select(r => new RoomResult(r.Id, r.Type, r.Price, r.Capacity)).ToList());
            case "Receptionist":
                return Ok(rooms.Select(r => new RoomResult(r.Id, r.Type, r.Price, r.State, r.Capacity, r.IsAvailable, r.IsClean)).ToList());
            default:
                return BadRequest();
        };
    }

    //retourne les chambres non nettoyé
    [CustomAuthorization(UserRole.Housekeeper)]
    [HttpGet("unclean")]
    public IActionResult ShowUncleanRooms()
    {
        return Ok(_roomsService.Select(r => !r.IsClean));
    }

    //change le status d'une chambre a nettoyé
    [CustomAuthorization(UserRole.Housekeeper)]
    [HttpPatch("clean/{roomId}")]
    public IActionResult CleanRoom(string roomId)
    {
        _roomsService.CleanRoom(roomId);
        return Ok("Room cleaned");
    }
}