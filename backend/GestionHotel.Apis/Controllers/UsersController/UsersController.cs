﻿using GestionHotel.Apis.Models;
using GestionHotel.Apis.Services;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace GestionHotel.Apis.Controllers;

[ApiController]
[Route("api/[controller]")]
public class UsersController : Controller
{
    private UserService userService = new UserService();

    //creer un utilisateur
    [HttpPost("signin")]
    public IActionResult Register([FromBody] RegistrationInput input)
    {
        var user = new User(input.LastName, input.FirstName, input.Email, input.Password);
        if (userService.RegisterAccount(user))
            return Ok("User created");
        else
            return BadRequest("Lacks informations");
    }

    //recupere un utilisateur via ces infos
    [HttpGet("signup")]
    public UserWithoutPassword Login([FromBody] LoginInput input)
    {
        return userService.Login(input.Email, input.Password);
    }
}