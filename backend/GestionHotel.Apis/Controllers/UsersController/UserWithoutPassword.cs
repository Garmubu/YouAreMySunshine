﻿using GestionHotel.Apis.Models.Enums;

namespace GestionHotel.Apis.Models;

public class UserWithoutPassword(string userId, string lastName, string firstName, string email, UserRole role)
{
    public string Id { get; set; } = userId;

    public string LastName { get; set; } = lastName;

    public string FirstName { get; set; } = firstName;

    public string Email { get; set; } = email;

    public UserRole Role { get; set; } = role;
}