﻿using GestionHotel.Apis.Models;

namespace GestionHotel.Apis.Services;

public class UserService : GenericCrudService<User>
{
    private readonly DatabaseContext _db = new();

    public bool ApplyUserVerifications(User user)
    {
        if (user.Password.Length < 10) return false;
        var addr = new System.Net.Mail.MailAddress(user.Email);
        if (user.Email != addr.Address) return false;
        int foundUserCount = Select(u => u.Email == user.Email).Count;
        return foundUserCount == 0;
    }
    
    public bool RegisterAccount(User user)
    {
        if (!ApplyUserVerifications(user)) return false;
        _db.Users.Add(user);
        _db.SaveChanges();
        return true;
    }

    public UserWithoutPassword Login(string email, string password)
    {
        var user = _db.Users.First(u => u.Email == email);
        if (user == null) new BadHttpRequestException("Cannot find User.", 404);
        if (user.Password != password) new BadHttpRequestException("Please check your credentials.", 401);
        var finalUser = new UserWithoutPassword(user.Id, user.Email, user.LastName, user.FirstName, user.Role);
        return finalUser;
    }
    
}