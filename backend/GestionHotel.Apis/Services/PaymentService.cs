﻿using GestionHotel.Apis.Models;
using GestionHotel.Apis.Models.Enums;
using GestionHotel.Externals.PaiementGateways.Paypal;
using GestionHotel.Externals.PaiementGateways.Stripe;

namespace GestionHotel.Apis.Services;

public class PaymentService
{
    private readonly PaypalGateway _paypalGateway = new();
    private readonly StripeGateway _stripeGateway = new();
    
    public void HandlePayment(Payment payment)
    {
        
        switch (payment.PaymentMethod)
        {
            case PaymentMethod.Paypal:
                HandlePaypalPayment(payment);
                break;
            case PaymentMethod.Stripe:
            {
                HandleStripePayment(payment);
                break;
            }
            case PaymentMethod.Other:
                break;
            default:
                throw new ArgumentException(nameof(payment.PaymentMethod), payment.PaymentMethod.ToString(), null);
        }
    }

    private void HandlePaypalPayment(Payment payment)
    {
        _paypalGateway.ProcessPaymentAsync(payment.CreditCard, payment.ExpiryDate, payment.Amount);
    }

    private void HandleStripePayment(Payment payment)
    {
        var stripePayementInfo = new StripePayementInfo
        {
            CardNumber = payment.CreditCard,
            ExpiryDate = payment.ExpiryDate,
            Amount = payment.Amount
        };
        _stripeGateway.ProcessPaymentAsync(stripePayementInfo);
    }
}