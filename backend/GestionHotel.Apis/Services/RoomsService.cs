﻿using GestionHotel.Apis.Models;

namespace GestionHotel.Apis.Services;

public class RoomsService : GenericCrudService<Room>
{
    public void CleanRoom(string roomId)
    {
        var room = this.SelectById(roomId);
        if (room == null) new ArgumentNullException("Unknow room");
        room.CleanRoom();
        this.Update(room);
    }

    public void MarkRoomForCleaning(string roomId)
    {
        var room = this.SelectById(roomId);
        if (room == null) new ArgumentNullException("Unknow room");
        room.MarkRoomForCleaning();
        this.Update(room);
    }

    public void ChangeRoomAvailability(string roomId, bool available)
    {
        var room = this.SelectById(roomId);
        if (room == null) new ArgumentNullException("Unknow room");
        room.ChangeRoomAvailability(available);
        this.Update(room);
    }
    
    public void ChangeRoomOccupation(string roomId, bool occupied)
    {
        var room = this.SelectById(roomId);
        if (room == null) new ArgumentNullException("Unknow room");
        room.ChangeRoomOccupation(occupied);
        this.Update(room);
    }
}