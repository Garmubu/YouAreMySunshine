﻿using GestionHotel.Apis.Controllers;
using GestionHotel.Apis.Models;
using GestionHotel.Apis.Models.Enums;

namespace GestionHotel.Apis.Services;

public class BookingsService : GenericCrudService<Booking>
{
    private readonly RoomsService _roomsService = new();
    private readonly UserService _userService = new();
    private readonly PaymentService _paymentService = new();
    private readonly MailerService _mailerService = new();
    public void AttemptBooking(BookingReservationInput input)
    {
        var room = _roomsService.SelectById(input.RoomId);
        if (room == null) new ArgumentNullException("Room is null.");
        if (!room.IsAvailable) new ArgumentException("Room is taken.");
        BookRoom(input, room);
    }

    
    private void BookRoom(BookingReservationInput input, Room room)
    {
        var booking = new Booking(input.ClientId, input.StartDate, input.EndDate, room.Id);
        _roomsService.ChangeRoomAvailability(room.Id, false);
        if (input.PaymentMethod != PaymentMethod.Other)
        {
            booking = UpdateBookingPaymentMethodAndApplyPayment(booking, input.Payment);
        }
        Insert(booking);
    }

    private Booking UpdateBookingPaymentMethodAndApplyPayment(Booking booking, Payment payment)
    {
        _paymentService.HandlePayment(payment);
        booking.PayBooking();
        return booking;
    }
    public Booking FindBookingForClientByEmail(string email)
    {
        var user = _userService.Select(u => u.Email == email)[0]!;
        return Select(b => b.UserId == user.Id)[0];
    }

    public void HandleClientArrival(string email, Payment? payment)
    {
        var user = _userService.Select(u => u.Email == email)[0]!;
        var booking = Select(b => b.UserId == user.Id)[0];
        if (booking == null) return;
        if (payment != null && booking.PaymentStatus != PaymentStatus.Paid)
        {
            booking = UpdateBookingPaymentMethodAndApplyPayment(booking, payment);
            Update(booking);
        }
        _roomsService.ChangeRoomOccupation(booking.RoomId, true);
    }

    public void HandleClientDeparture(string email, Payment payment)
    {
        var booking = FindBookingForClientByEmail(email);
        booking = UpdateBookingPaymentMethodAndApplyPayment(booking, payment);
        _roomsService.MarkRoomForCleaning(booking.RoomId);
        _roomsService.ChangeRoomAvailability(booking.RoomId, true);
        _roomsService.ChangeRoomOccupation(booking.RoomId, false);
        var user = _userService.SelectById(booking.UserId);
        if (user == null) return;
        _mailerService.SendEmail(user.Email, "Thank you for staying.");
    }
    
    public void CancelBooking(string bookingId)
    {
        var booking = SelectById(bookingId);
        if (booking == null) return;
        booking.BookingStatus = BookingStatus.Canceled;
        if ((booking.StartDate - DateTime.Now).TotalDays < 2)
        {
            Console.WriteLine("Client cancelled 2 days before the date, no refund.");
        }
        _roomsService.ChangeRoomAvailability(booking.RoomId, true);
        Update(booking);
    }
}