namespace GestionHotel.Apis.Services;

public class MailerService
{
    public void SendEmail(string email, string subject)
    {
        Console.WriteLine($"Sending {subject} to {email}...");
    }
}